package com.logicline.application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.validator.UrlValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.testng.IConfigurationListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;
import org.testng.xml.XmlTest;

import com.lenze.pages.SalesforceLoginPage;
import com.logicline.expections.DataSheetException;
import com.logicline.utils.DataSheetReader;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AutomationToolController {

	@FXML
	public TextField URL;

	@FXML
	public TextField USERNAME;

	@FXML
	public TextField PASSWORD;

	@FXML
	public Button VERIFY;

	@FXML
	public Label STATUS;

	@FXML
	public TextField Emailid;

	@FXML
	public ComboBox<String> browserSelect;

	@FXML
	private AnchorPane TestCases;
	
	@FXML
	private AnchorPane browserPane;


	@FXML
	private CheckBox SENDEMAIL;

	@FXML
	private Label EMAIL_LABEL;

	@FXML
	private TitledPane root;

	@FXML
	private Label URL_ERROR_MSG;

	@FXML
	public Label VALIDATION_ERROR_MSG;

	@FXML
	private Label EMAIL_ERROR_MSG;
	
	@FXML
	private Button Execute;

	@FXML
	ImageView spinnerImage;

	@FXML
	TabPane tabPane;
	
	@FXML
	public CheckBox BACK_RUN;
	
	@FXML
	public CheckBox GOOGLECHROMECHECKBOX;
	
	@FXML
	public CheckBox FIREFOXCHECKBOX;
	
	@FXML
	public CheckBox EXECUTEPARALLEL;

	String Path = null;
	String userName = null;
	String password = null;
	String receiverEmailID = null;
	String browser = null;
	String setbackrun="false";
	public boolean sendEmailstatus = false;
	private boolean isParallelSelected =false;

	private CheckBox checkBox = null;
	private List<CheckBox> checkBoxList = new ArrayList<CheckBox>();
	private DataSheetReader dataSheetReader = null;
	private LinkedHashMap<String, String> testCases = null;
	private int numberOfTestCases = 0;
	private WorkIndicatorDialog wd = null;
    private WebDriver driver;
    private HashMap<Integer, String> browserMap;

	public AutomationToolController() {

		// Constructor of controller class,We are initializing all controls in fxml
		USERNAME = new TextField();
		URL = new TextField();
		PASSWORD = new PasswordField();
		VERIFY = new Button();
		STATUS = new Label();
		Emailid = new TextField();
		browserSelect = new ComboBox();
		SENDEMAIL = new CheckBox();
		EMAIL_LABEL = new Label();
		
		

	}

	@FXML
	public boolean getData() {

		if (isNotEmpty()) {
			Path = URL.getText().trim();
			userName = USERNAME.getText().trim();
			password = PASSWORD.getText().trim();
			receiverEmailID = Emailid.getText().trim();
			browser = browserSelect.getValue();
			System.out.println("Browser" + browser);
			return true;

		}

		else {
			return false;
		}

	}

	@FXML
	public void verifyCredentials_Click(ActionEvent event) {

	//	System.setProperty("phantomjs.binary.path", "Drivers\\phantomjs.exe");
		//final WebDriver driver = new PhantomJSDriver();

		VERIFY.setDisable(true);
		/*
		 * spinnerImage.setVisible(true);
		 * 
		 * spinnerImage.requestFocus(); System.out.println(spinnerImage.isFocused());
		 */

		// ImageView image =new ImageView("Eclipse-1s-200px.gif");
		if (!URL.getText().trim().isEmpty() && !USERNAME.getText().trim().isEmpty()
				&& !PASSWORD.getText().trim().isEmpty()) {

	
			if (urlAuthentication()) {
				root.setDisable(true);
				inititatePhatomJS();
				final WebDriver driver=getPhatomJSDriver();
				wd = new WorkIndicatorDialog(root.getScene().getWindow(), "Verifying Credentials ...");

				wd.addTaskEndNotification(result -> {
					
					System.out.println(result);
					if (result.equals(1)) {
						System.out.println("Login Sucess");
						Alert alert = new Alert(Alert.AlertType.INFORMATION);
						alert.setTitle("Verify Credentials");
						alert.setHeaderText("Credentials verified successfully");
						alert.showAndWait();
						VERIFY.setDisable(false);
						driver.quit();
						root.setDisable(false);

					}

					else if (result.equals(2))

					{
						System.out.println("Login Failed");
						Alert alert = new Alert(Alert.AlertType.ERROR);
						alert.setTitle("Verify Credentials");
						alert.setHeaderText("Credentials verification failed");
						alert.showAndWait();
						VERIFY.setDisable(false);
						driver.quit();
						root.setDisable(false);
					}

					else if (result.equals(3)) {
						System.out.println("Only saleforce");
						Alert alert = new Alert(Alert.AlertType.ERROR);
						alert.setTitle("INVALID URL");
						alert.setHeaderText("Sorry!!Currently only salesforce URL is supporeted");
						alert.showAndWait();

						VERIFY.setDisable(false);
						root.setDisable(false);
					}
				});
				
				
					wd.exec("123", inputParam -> 
					{

						try {

							if (getData()) {

								driver.get(Path);
								if (driver.getTitle().contains("Salesforce")) {

									SalesforceLoginPage login_obj = new SalesforceLoginPage(driver);
									login_obj.saleforceLogin(userName, password);
									Thread.sleep(10000);

									if (driver.findElements(By.xpath("//DIV[@id='error']")).isEmpty()) {

										return new Integer(1);

									} else {
										System.out.println(driver.getCurrentUrl());
										return new Integer(2);

									}

								} else {

									return new Integer(3);
								}
							} else {

								return new Integer(4);
							}
						}

						catch (Exception e) {
							driver.quit();
							e.printStackTrace();
							return new Integer(6);
						}
						// return new Integer(6);
					});
					wd = null;
				
		
			} else {
				URL_ERROR_MSG.setText("Invalid URL");
				VERIFY.setDisable(false);
			}

		} else {
			VALIDATION_ERROR_MSG.setText("Please enter values in all fields!!");
			VERIFY.setDisable(false);
		}

		// don't keep the object, cleanup

	

		// TestCases.setDisable(true);

	}

	@FXML
	public void Save_Click(ActionEvent event) throws IOException {
		
		if(EXECUTEPARALLEL.isSelected())
		{
		 browserMap=new HashMap<Integer, String>();
		
		
		for (int i=0; i<browserPane.getChildren().size();i++)
		{
System.out.println(browserPane.getChildren().get(i));
if(browserPane.getChildren().get(i) instanceof CheckBox)
{
if(((CheckBox) browserPane.getChildren().get(i)).isSelected())
browserMap.put(i,((CheckBox) browserPane.getChildren().get(i)).getText() );

}
		}
		System.out.println(browserMap);
		isParallelSelected=true;
		}
		else
		{
			isParallelSelected=false;
		}

		if (getData()) {
			if (urlAuthentication()) {
				if (SENDEMAIL.isSelected()) {
					if (emailAuthentication()) {
						sendEmailstatus = true;
						update_properties(sendEmailstatus,setbackrun);
					}

					else {
						EMAIL_ERROR_MSG.setText("Invalid email address");
					}
				} else {
					update_properties(sendEmailstatus,setbackrun);

				}

			} else {
				URL_ERROR_MSG.setText("Invalid URL");

			}
		}

		else {
			VALIDATION_ERROR_MSG.setText("Please enter values in all fields!!");

		}
	}

	@FXML
	public void cencelButton_Click(ActionEvent event) throws IOException {

		URL.setText("");
		USERNAME.setText("");
		PASSWORD.setText("");
		Emailid.setText("");
	}

	private boolean emailAuthentication() {
		InternetAddress emailAddr;
		String emailListArray[] = Emailid.getText().split(",");
		boolean emailFlag = true;
		for (String email : emailListArray) {
			try {
				emailAddr = new InternetAddress(email);
				emailAddr.validate();

			} catch (AddressException e) {
				emailFlag = false;

			}

		}
		return emailFlag;

	}

	private boolean urlAuthentication() {
		boolean urlFlag = true;
		String url = URL.getText();
		String[] schemes = { "http", "https", "www" };
		// DEFAULT schemes = "http", "https", "ftp";
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (urlValidator.isValid(url)) {
			return urlFlag;

		} else {
			urlFlag = false;
		}

		return urlFlag;
	}

	public void initialize() throws DataSheetException {
		getTestCases();
		constructCheckBoxes(testCases, numberOfTestCases);
		loadDefaultValue();
	}

	private void getTestCases() throws DataSheetException {
		dataSheetReader = new DataSheetReader("C:\\Users\\Teny\\Documents\\lenze-automation\\Settings\\TestDataSheet.xlsx");
		testCases = dataSheetReader.getTestCases();
		numberOfTestCases = testCases.size();
	}

	private void constructCheckBoxes(LinkedHashMap<String, String> testCases, int numberOfTestCases) {
		int checkBoxGap = 0;

		for (String key : testCases.keySet()) {
			checkBox = new CheckBox(key + ". " + testCases.get(key));
			checkBox.setOnAction(checkBoxHandler);
			checkBox.setLayoutX(5);
			checkBox.setLayoutY(checkBoxGap + 5);
			checkBox.setPadding(new Insets(0, 5, 0, 0));
			checkBoxGap = checkBoxGap + 50;
			TestCases.getChildren().add(checkBox);
			checkBoxList.add(checkBox);
		}

		/*
		 * for(int i=0; i<numberOfTestCases; i++) {
		 * 
		 * checkBox = new CheckBox(testCaseList.get(i)); checkBox.setLayoutX(5);
		 * checkBox.setLayoutY(checkBoxGap+5); checkBox.setPadding(new Insets(0, 5, 0,
		 * 0)); checkBoxGap = checkBoxGap + 50; TestCases.getChildren().add(checkBox);
		 * 
		 * }
		 */
	}
	EventHandler<ActionEvent> checkBoxHandler = new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent event) {
	        for(CheckBox checkBox: checkBoxList) {
	        	if(checkBox.isSelected()) {
	        		Execute.setDisable(false);
	        		break;
	        	
	        	} else {
	        		Execute.setDisable(true);
	        	}
	        	
	        }
	    }
	};
	@SuppressWarnings({ "deprecation", "null" })
	@FXML


	void executeTestCases() throws DataSheetException {
		List<String> testCaseIndexList = new ArrayList<String>();
		for (CheckBox checkBox : checkBoxList) {
			if (checkBox.isSelected()) {
				String testCase = checkBox.getText();
				testCaseIndexList.add(testCase.substring(0, testCase.indexOf(".")));
				// System.out.println(testCase.substring(0, testCase.indexOf(".")));
			}
		}

		dataSheetReader.setTestCases(testCaseIndexList);

/*		TestListenerAdapter tla = new TestListenerAdapter();

		TestNG testng = new TestNG();
		List<String> suites = Lists.newArrayList();
		suites.add("App Settings/testng.xml");
		final XmlClass xmlClass = new XmlClass("com.lenze.suite.Smoke");
		List<XmlClass> xmlClasses = new ArrayList<XmlClass>()  ;
		xmlClasses.add(xmlClass);

		XmlSuite suite = new XmlSuite();
		suite.setName("Suite");
		suite.setParallel(ParallelMode.TESTS);
		XmlTest test = new XmlTest();
		test.setName("Test1");
		test.setClasses(xmlClasses);
		//test.setParameters((Map<String, String>) new HashedMap().put("testBrowser", "Google Chrome"));
		suite.addTest(test);
		XmlTest test2 = new XmlTest(suite);
		test2.setName("Test1");
		test2.setClasses(xmlClasses);
		test2.setParameters((Map<String, String>) new HashedMap().put("testBrowser", "Mozilla Firefox"));*/
		XmlSuite suite = new XmlSuite();
		suite.setName("TmpSuite");
		 
		
		if(isParallelSelected)
		{
			suite.setParallel(ParallelMode.TESTS);
			for(int i=0;i<browserMap.size();i++)
			{
		XmlTest test = new XmlTest(suite);
		test.setName("TmpTest" + i);
		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass("com.lenze.suite.Smoke"));
		test.setXmlClasses(classes) ;
		  Map<String, String> parameters = new HashMap<String, String>();
		  parameters.put("testBrowser", browserMap.get(i));
		test.setParameters(parameters);
			}
		}
		
		else
		{
			suite.setParallel(ParallelMode.TESTS);
			XmlTest test = new XmlTest(suite);
			test.setName("TmpTest");
			List<XmlClass> classes = new ArrayList<XmlClass>();
			classes.add(new XmlClass("com.lenze.suite.Smoke"));
			test.setXmlClasses(classes) ;
			  Map<String, String> parameters = new HashMap<String, String>();
			  parameters.put("testBrowser", browserSelect.getValue());
			test.setParameters(parameters);
		}
		/*XmlTest test2 = new XmlTest(suite);
		test2.setName("TmpTest2");
		
		
		test2.setXmlClasses(classes) ;
	 parameters = new HashMap<String, String>();
		  parameters.put("testBrowser", "Google Chrome");
		  test2.setParameters(parameters);*/
//testng.addListener((IConfigurationListener)tla);
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);

/*List suites = new ArrayList();
suites.add(suite);
System.out.println(test);*/
		//testng.setTestSuites(suites);
	//	testng.setXmlSuites(suites);
		wd = new WorkIndicatorDialog(root.getScene().getWindow(), "Executing ...");
		wd.addTaskEndNotification(result -> {
			
			
	
			while (result.equals(1)) {
		
				root.setDisable(false);

			}

		});
		
		wd.exec("1234", inputParam -> 
		{
int integer=0;
			try {	
				 Thread t=  new Thread() {
			            public void run() {
			            	try
			            	{
			            		System.out.println(tng.getDefaultSuiteName());
			            		tng.run();
			            	
			        /*     	//System.out.println(tla.getTestContexts().size());
			            	//com.empolis.suite.Smoke s=(Smoke) tla.getConfigurationFailures().get(0).getInstance();
			          	System.out.println( tla.getConfigurationFailures().get(0).getInstance().getClass().getDeclaredFields()[0]);
			            	//System.out.println(tla.getConfigurationFailures());
*/			            	}
			            	catch (Exception e)
			            	{
			            		System.out.println(e.getMessage());
			            	}
			            }
			        };
			t.start();
				while (t.isAlive())
				{
			//	return integer;
			}
				if(!t.isAlive())
				{
					return integer;
				}
				
			}
			catch (Exception e) {
			e.printStackTrace();
			return new Integer(6);
			}
			 return integer;

		});
		wd = null;


	

	}

	@FXML
	public void SendEmail_Checkbox(ActionEvent event) {
		if (SENDEMAIL.isSelected()) {
			Emailid.setVisible(true);
			;
			EMAIL_LABEL.setVisible(true);

			System.out.println("checked");

		}

		else {
			Emailid.setVisible(false);
			;
			EMAIL_LABEL.setVisible(false);
			System.out.println("Unchecked");
			sendEmailstatus = false;
		}

	}

	public void update_properties(boolean UdateEmail_status,String setbackrun) throws IOException {
		FileInputStream in = new FileInputStream("./App Settings/config.properties");
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream("./App Settings/config.properties");

		props.setProperty("testSiteLadingPageURL", Path);
		props.setProperty("username", userName);
		props.setProperty("password", password);
		props.setProperty("browser", browser);
		props.setProperty("enablebackrun", setbackrun);

		if (UdateEmail_status) {
			props.setProperty("receiverId", receiverEmailID);
			props.setProperty("sendMail", "Yes");
		} else {
			props.setProperty("sendMail", "No");
		}
		props.store(out, null);
		out.close();
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Setting updated success");
		alert.setHeaderText("Settings updated successfully");
		alert.showAndWait();
	}

	public boolean isNotEmpty() {
		if (URL.getText().trim().isEmpty() || USERNAME.getText().trim().isEmpty()
				|| PASSWORD.getText().trim().isEmpty()) {

			System.out.println("empty");
			return false;

		}

		else {
			return true;
		}
	}

	private void loadDefaultValue() {
		try {
			FileInputStream in = new FileInputStream("./App Settings/config.properties");
			Properties props = new Properties();
			props.load(in);
			in.close();
			if (!props.getProperty("username").trim().isEmpty()) {
				USERNAME.setText(props.getProperty("username"));
			}

			if (!props.getProperty("password").trim().isEmpty()) {
				PASSWORD.setText(props.getProperty("password"));
			}

			if (!props.getProperty("testSiteLadingPageURL").trim().isEmpty()) {
				URL.setText(props.getProperty("testSiteLadingPageURL"));
			}

			if (!props.getProperty("receiverId").trim().isEmpty()) {
				Emailid.setText(props.getProperty("receiverId"));
			}
			
			if (props.getProperty("sendMail").trim().equalsIgnoreCase("yes")) {
				
				SENDEMAIL.setSelected(true);
				Emailid.setVisible(true);
	
				
			}
	if (Boolean.valueOf(props.getProperty("enablebackrun").trim())) {
				
				BACK_RUN.setSelected(true);
	
				
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@FXML
	void Additional_Settings() throws IOException 
	{
		 FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AdditionalSettingsFXML.fxml"));
         Parent root1 = (Parent) fxmlLoader.load();
         Stage stage = new Stage();
         stage.initModality(Modality.APPLICATION_MODAL);
         //stage.initStyle(StageStyle);
         stage.setTitle("Additional Settings");
         stage.resizableProperty().set(Boolean.FALSE);
         stage.setScene(new Scene(root1));  
         stage.show();
	}

	@FXML
	void clearErrorMessages(Event event) {
		/* System.out.println(event.getEventType()); */
		URL_ERROR_MSG.setText("");
		VALIDATION_ERROR_MSG.setText("");
		EMAIL_ERROR_MSG.setText("");

	}
	
	@FXML
	void viewReport() throws IOException 
	{
		 FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ReportViewFXML.fxml"));
         Parent root1 = (Parent) fxmlLoader.load();
         Stage stage = new Stage();
         stage.initModality(Modality.APPLICATION_MODAL);
         //stage.initStyle(StageStyle);
         stage.setTitle("Report View");
         stage.resizableProperty().set(Boolean.FALSE);
         stage.setScene(new Scene(root1));  
         stage.show();
	}
	@FXML
	void Execute_Parallel() throws IOException 
	{
		if(EXECUTEPARALLEL.isSelected())
		{
			browserSelect.setVisible(false);
			GOOGLECHROMECHECKBOX.setVisible(true);
			FIREFOXCHECKBOX.setVisible(true);
			
	
			
		}
		else
		{
			browserSelect.setVisible(true);
			GOOGLECHROMECHECKBOX.setVisible(false);
			FIREFOXCHECKBOX.setVisible(false);
		}
	}
	
	public void inititatePhatomJS()
	{
		System.setProperty("phantomjs.binary.path", "App Settings/Drivers/phantomjs.exe");
		this.driver=new PhantomJSDriver();
	}
	public WebDriver getPhatomJSDriver()
	{
		return this.driver;
	}
	
	public void backRunClick(ActionEvent event)
	{
		if(BACK_RUN.isSelected())
		{
			setbackrun="true";
		}
		
		else
		{
			setbackrun="false";
			
		}
	}
}

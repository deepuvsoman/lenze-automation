package com.logicline.application;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class ReportViewController {
	@FXML
	ListView<String> reportListView;

	@FXML
	Pane root;

	public void initialize() {

		File folder = new File("./Tests/Reports");
		File[] listOfFiles = folder.listFiles();
		ObservableList data = FXCollections.observableArrayList();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());
				data.add(listOfFiles[i].getName());

			}

		}

		final ListView listView = new ListView(data);

		listView.prefWidthProperty().bind(root.widthProperty());
		root.getChildren().add(listView);
		listView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {

					try {
						File htmlFile = new File("Tests\\Reports\\" + listView.getSelectionModel().getSelectedItem());
				        System.out.println(htmlFile);
						
						Desktop.getDesktop().browse(htmlFile.toURI());

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

		});

	}

}

package com.logicline.application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashController implements Initializable {

	@FXML
	private StackPane rootPane;
	
	@FXML
	ImageView imageView;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		new SplashScreen().start();

	}

	class SplashScreen extends Thread {

		public void run() {
			try {
				Thread.sleep(3000);

				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						Parent root = null;
						try {
							root = FXMLLoader.load(getClass().getResource("AutomationTool.fxml"));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Scene scene = new Scene(root);
						scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
						Stage primaryStage = new Stage();
						imageView.fitWidthProperty().bind(primaryStage.widthProperty());
						imageView.fitHeightProperty().bind(primaryStage.heightProperty());
						primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("ll-cloud-16.png")));
						primaryStage.setTitle("Logicline Automation Tool");
						primaryStage.setScene(scene);
						primaryStage.resizableProperty().setValue(Boolean.FALSE);
						primaryStage.show();

						rootPane.getScene().getWindow().hide();

					}
				});

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

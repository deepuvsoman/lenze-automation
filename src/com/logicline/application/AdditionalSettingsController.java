package com.logicline.application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import com.logicline.expections.DataSheetException;
import com.logicline.utils.EncriptData;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AdditionalSettingsController {

	@FXML
	private GridPane AdditionalSettings;

	@FXML
	private GridPane emailsettingsGrid;

	@FXML
	private GridPane reportSettingsGrid;
	@FXML
	private GridPane saleforSettingsGrid;

	@FXML
	TitledPane root;
	
	@FXML
	public Button CANCEL_BUTTON;
	
	@FXML
	public Button SAVE;

	private Label key = null;
	private TextField value = null;
	private int rowCount = 1;
	private int columnCount = 2;
	private int increment = 0;
	private String pwd;
	private String[] keyNotIncluded = { "username", "receiverId", "password", "browser", "sendMail",
			"testSiteLadingPageURL","enablebackrun" };

	private String[] emailKeys = { "senderPwd", "senderID", "mailBody", "mailSubject" };

	private String[] reportKeys = { "projectDescription", "projectName" };

	private String[] salesforceKeys = { "client_id", "cleint_secret", "tokenurl" };

	HashMap<String, String> labels = new HashMap<String, String>();

	public void initialize() throws Exception {
		ScrollPane scrollPane = new ScrollPane(AdditionalSettings);
		scrollPane.setFitToHeight(true);

		/*
		 * AdditionalSettings.setPadding(new Insets(10));
		 * AdditionalSettings.setSpacing(8);
		 */;
		labels.put("senderPwd", "Sender Password");
		labels.put("senderID", "Sender Mail ID");
		labels.put("cleint_secret", "Client Secret");
		labels.put("mailBody", "Mail Body Content");
		labels.put("mailSubject", "Mail Subject");
		labels.put("projectDescription", "Project Description");
		labels.put("projectName", "Project Name");
		labels.put("client_id", "Client Id");
		labels.put("tokenurl", "Token URL");
		getAddiitonalSettings();

	}

	private void getAddiitonalSettings() throws Exception {
		FileInputStream in;
		try {
			in = new FileInputStream("./App Settings/config.properties");

			Properties props = new Properties();
			props.load(in);
			in.close();
			Set<Object> keySet = props.keySet();
			int emailRow = 0, reportRow = 0, salesforRow = 0;
			for (Iterator<Object> it = keySet.iterator(); it.hasNext();) {
				

				String keyValue = it.next().toString();
			
				String labelName = labels.get(keyValue);
				 if(labelName=="Sender Password")
	                {
					key = new Label(labelName);
					value = new PasswordField();
					value.setId(keyValue);
	                }
	                else
	                {
	                	key = new Label(labelName);
	    				value = new TextField();
	    				value.setId(keyValue);
	                }


				if (!Arrays.asList(keyNotIncluded).contains(keyValue)) {
				

					if (Arrays.asList(emailKeys).contains(keyValue)) {
						emailsettingsGrid.add(key, 0, emailRow);
						emailsettingsGrid.add(value, 1, emailRow);
						emailRow++;
					} else if (Arrays.asList(reportKeys).contains(keyValue)) {
						reportSettingsGrid.add(key, 0, reportRow);
						reportSettingsGrid.add(value, 1, reportRow);
						reportRow++;
					}

					else if (Arrays.asList(salesforceKeys).contains(keyValue)) {
						saleforSettingsGrid.add(key, 0, salesforRow);
						saleforSettingsGrid.add(value, 1, salesforRow);
						salesforRow++;
					}
					if (!props.getProperty(keyValue).trim().isEmpty()) {
						
						pwd=props.getProperty(keyValue).trim();
						if("senderPwd".equals(keyValue))
						{
						EncriptData obj=new EncriptData();
						pwd=obj.decrypt(pwd);
						}
                        value.setText(pwd);
					}
				
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
    void additionalSave() {

		FileInputStream in;
		try {
			in = new FileInputStream("./App Settings/config.properties");

			Properties props = new Properties();
			props.load(in);
			in.close();
			Set<Object> keySet = props.keySet();

			for (Iterator<Object> it = keySet.iterator(); it.hasNext();) {
				String keyValue = it.next().toString();
				if (!Arrays.asList(keyNotIncluded).contains(keyValue)) {
					System.out.println("#" + keyValue);
					System.out.println(((TextField) root.lookup("#" + keyValue)).getText());
					update_properties(keyValue, ((TextField) root.lookup("#" + keyValue)).getText());
				}
			}

			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Setting updated success");
			alert.setHeaderText("Settings updated successfully");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Stage stage = (Stage) root.getScene().getWindow();

				stage.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void update_properties(String key, String Value) throws Exception {
		FileInputStream in = new FileInputStream("./App Settings/config.properties");
		Properties props = new Properties();
		props.load(in);
		in.close();
		FileOutputStream out = new FileOutputStream("./App Settings/config.properties");

		if("senderPwd".equals(key))
        {
			if(Value!="" && Value!=null)
			{
        	EncriptData obj=new EncriptData();
        	Value=obj.encrypt(Value);
			}
        }
		
		props.setProperty(key, Value);

		props.store(out, null);
		out.close();
		/*
		 * Alert alert = new Alert(Alert.AlertType.INFORMATION);
		 * alert.setTitle("Setting updated success");
		 * alert.setHeaderText("SETTINGS UPDATED SUCCESSFULLY"); alert.showAndWait();
		 */
	}
	
	@FXML
	void cancelButton()
	{
		Stage stage = (Stage) CANCEL_BUTTON.getScene().getWindow();
	    stage.close();
	}
}
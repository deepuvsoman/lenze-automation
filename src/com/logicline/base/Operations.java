package com.logicline.base;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import org.apache.log4j.Logger;

import com.logicline.expections.AutomationException;
import com.logicline.utils.Log;



public abstract class Operations {
	protected WebDriver driver = null;
	protected final Logger logger = Log.getInstance(this.getClass().getSimpleName());
	private WebDriverWait wait = null;
	protected WebElement element = null;
	protected String elementText = null;
	
	public enum LocatorType {BY_XPATH, BY_LINKTEXT, BY_ID, BY_CLASSNAME,	BY_NAME, BY_CSSSELECTOR, BY_PARTIALLINKTEXT, BY_TAGNAME };
	
	protected Operations(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
	}
	
	/**
	 * This Method is to get the By object based on the locator type and value
	 * @author Augustine
	 * @param locatorType
	 * 					- Locator Type with which By object to be identified
	 * @param locatorValue
	 * 					- Locator Value with which By object to be identified
	 * @return By object based on the locator type and value
	 * @throws AutomationException 
	 */	 
	protected By getLocator(LocatorType locatorType, String locatorValue) {		
		if(locatorType == LocatorType.BY_ID) {
			return By.id(locatorValue);
		} else if(locatorType == LocatorType.BY_NAME) {
			return By.name(locatorValue);
		} else if(locatorType == LocatorType.BY_LINKTEXT) {
			return By.linkText(locatorValue);
		} else if(locatorType == LocatorType.BY_PARTIALLINKTEXT) {
			return By.partialLinkText(locatorValue);
		} else if(locatorType == LocatorType.BY_CSSSELECTOR) {
			return By.cssSelector(locatorValue);
		} else if(locatorType == LocatorType.BY_CLASSNAME) {
			return By.className(locatorValue);
		} else if(locatorType == LocatorType.BY_TAGNAME) {
			return By.tagName(locatorValue);
		} else if(locatorType == LocatorType.BY_XPATH) {
			return By.xpath(locatorValue);
		} else {
			throw new IllegalArgumentException("Invalid Locator Type");
		}
	}
		
	
	/**
	 * This method loads the given URL in the current browser window.
	 * @author Augustine
	 * @param URL
	 * 			- URL to be loaded
	 * @throws AutomationException
	 */
	public void loadURL(String URL) throws AutomationException {
		//String URL = BaseTest.getApplicationURL();
		if (URL.length()!=0) {
			try {
				driver.get(URL);
				logger.info("The URL: "+ URL +"is loaded");
			} catch(WebDriverException e) {
				logger.error("Unable to load the URL: " + URL + "due to WebDriver Exception: " + e.getLocalizedMessage());
				throw new AutomationException("Unable to load the URL: " + URL + "due to WebDriver Exception: " + e.getLocalizedMessage());
			} catch(Exception e) {
				logger.error("Unable to load the URL: " + URL + "due to Exception: " + e.getLocalizedMessage());
				throw new AutomationException("Unable to load the URL: " + URL + "due to Exception: " + e.getLocalizedMessage());
			}
		} else {
			logger.error("Unable to load the invalid URL: "+URL+" Please provide a valid URL");
			throw new AutomationException("Unable to load the invalid URL: "+URL+". Please provide a valid URL");
		}
	}

	/**
	 * This method refresh the current page
	 * @author Augustine
	 * @throws AutomationException 
	 * 
	 */
	public void refreshPage() throws AutomationException {
		try {
			driver.navigate().refresh();
			logger.info("The page is refreshed");
		} catch(WebDriverException e) {
			logger.error("The page cannot be refreshed due to WebDriver Exception: " + e.getLocalizedMessage());
			throw new AutomationException("The page cannot be refreshed due to WebDriver Exception: " + e.getLocalizedMessage());
		} catch(Exception e) {
			logger.error("The page cannot be refreshed due to Exception: " + e.getLocalizedMessage());
			throw new AutomationException("The page cannot be refreshed due to Exception: " + e.getLocalizedMessage());
		}
	}

	/**
	 * This method is to get an element in the Web page
	 * @author Augustine
	 * @param locator
	 *            - By object to locate the element
	 * @return the specified WebElement
	 * @throws AutomationException 
	 */
	public WebElement getElement(By locator) throws AutomationException {
		try {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			logger.info("The element with" + locator.toString().replace("By.", " ")	+ " is found");
		} catch(TimeoutException e){
			logger.error("Timeout Exception occuredwhile trying to get element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not visible");
		} catch(WebDriverException e) {
			logger.error("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to get element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to get element with " + locator.toString().replace("By.", ""));
		} catch(Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to get element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to get element with " + locator.toString().replace("By.", ""));
		}
		return element;
	}
	
	/**
	 * This method clicks on the element which can be located by the By Object
	 * @author Augustine
	 * @param locator
	 * 				- By object to locate the element to be clicked
	 * @throws AutomationException 
	 *            
	 */
	public void click(By locator) throws AutomationException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
			logger.info("The element with " + locator.toString().replace("By.", "")	+ " is clicked");
		} catch(StaleElementReferenceException e){
			logger.error("Stale Element Reference Exception occured while trying to click on element with "	+ locator.toString().replace("By.", ""));
			wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
			logger.info("The element with " + locator.toString().replace("By.", "")	+ " is clicked on the second try after Stale Element Reference Exception");		
		} catch(TimeoutException e){
			logger.error("Timeout Exception occured after waiting for the element with " + locator.toString().replace("By.", "")	+ " to be clickable");
			throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not clickable");
		} catch (WebDriverException e) {
			if(e.getMessage().contains("Element is not clickable at point")) {
				logger.error("Element with " + locator.toString().replace("By.", "")	+ " is not clickable");
				throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not clickable");
			} else {
				logger.error("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
				throw new AutomationException("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
			}
		} catch(Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
		}
	}

	/**
	 * This method is to enter input text into an input field in the web page
	 * @author Augustine           
	 * @param locator
	 * 				- By object to locate the input field to which text has to be entered           
	 * @param value
	 * 				- Text value which is to be send to the input field
	 * @throws AutomationException 
	 */
	public void enterText(By locator, String text) throws AutomationException {
		try { 
		element = wait.until(ExpectedConditions.elementToBeClickable(locator));
			element.click();
			element.clear();
			element.sendKeys(text);
			logger.info("Input: " + text + " is entered in the element with " + locator.toString().replace("By.", ""));
		} catch(StaleElementReferenceException e){
			logger.error("Stale Element Reference Exception occured while trying to interact with element with "	+ locator.toString().replace("By.", ""));
			wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
			logger.info("The element with " + locator.toString().replace("By.", "")	+ " is clicked on the second try after Stale Element Reference Exception");		
		} catch(TimeoutException e){
			logger.error("Timeout Exception occured after waiting for the element with " + locator.toString().replace("By.", "")	+ " to be clickable");
			throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not clickable");
		} catch (WebDriverException e) {
			if(e.getMessage().contains("Element is not clickable at point")) {
				logger.error("Element with " + locator.toString().replace("By.", "")	+ " is not clickable");
				throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not clickable");
			} else {
				logger.error("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
				throw new AutomationException("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
			}
		} catch (IllegalArgumentException e) {
			logger.error("The text to be entered in the element with "	+ locator.toString().replace("By.", "") + " is null, Please give a valid text");
			throw new AutomationException("The text to be entered in the element with "	+ locator.toString().replace("By.", "") + " is null, Please give a valid text");
		} catch(Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to click element with " + locator.toString().replace("By.", ""));
		}
	}

	/**
	 * This method is to check whether a WebElement is visible or not
	 * @author Augustine
	 * @param locator 
	 * 				- By object of the WebElement which is to be checked
	 * @return - returns true if the specified WebElement is visible, else it will return false
	 * @throws AutomationException 
	 */
	public boolean isElementVisible(By locator) throws AutomationException {		
		try {
			if(wait.until(ExpectedConditions.presenceOfElementLocated(locator)).isDisplayed()) {
				logger.info("The element with "+ locator.toString().replace("By.", "") + " is visible");
				return true;
			} else {
				logger.info("The element with "	+ locator.toString().replace("By.", "") + " is not visible");
				return false;
			}
		} catch(TimeoutException e) {
			logger.error("Timeout Exception occured after waiting for the element with " + locator.toString().replace("By.", "")	+ " to be present in DOM of a page");
			return false;
		} catch(ElementNotVisibleException e){
			logger.error("The element with " + locator.toString().replace("By.", "")	+ " is not visible");
			return false;
		} catch(WebDriverException e) {
			logger.error("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to check visibility of element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to check visibility element with " + locator.toString().replace("By.", ""));
		} catch(Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to check visibility element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to check visibility element with " + locator.toString().replace("By.", ""));
		}
	}

	/**
	 * This method is to get the visible text of an element in the Web Page
	 * @author Augustine
	 * @param locator
	 *            - By object to locate the element from which the text has to be taken
	 * @return returns the visible text of the specified element
	 * @throws AutomationException 
	 */
	public String getText(By locator) throws AutomationException {
		try {
			elementText = wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).getText().trim();
			logger.info("The value on the field with " + locator.toString().replace("By.", "")	+ " is obtained");	
		} catch(TimeoutException e){
			logger.error("Timeout Exception occured while trying to check visibility of element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Element with " + locator.toString().replace("By.", "") + " is not visible");
		} catch(WebDriverException e) {
			logger.error("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to get text of element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("WebDriver Exception: " + e.getLocalizedMessage() + " occured while trying to get text of element with " + locator.toString().replace("By.", ""));
		} catch(Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to get text of element with " + locator.toString().replace("By.", ""));
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to get text of element with " + locator.toString().replace("By.", ""));
		}
		return elementText;
	}
	
}

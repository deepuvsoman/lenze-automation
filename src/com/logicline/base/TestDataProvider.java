package com.logicline.base;

import java.util.LinkedHashMap;

import org.testng.annotations.DataProvider;

import com.logicline.expections.DataSheetException;
import com.logicline.utils.DataSheetReader;

public class TestDataProvider {

	DataSheetReader dataSheetReader = null;
	/*private static TestDataProvider instance;


	private TestDataProvider() {

	}
	public static TestDataProvider getInstance() {
		if(instance == null) {
			instance = new TestDataProvider();
		}
		return instance;
	}*/
	
	/**
	 * This method returns the test data from excel sheet as the DataProvider
	 * @author Augustine
	 * @param externalSheetPath
	 * @param sheetName
	 * @return 2-dimensional object array for the DataProvider
	 * @throws DataSheetException 
	 */
	@DataProvider(name = "dataSheet")
	public Object[][] getTestDataFromExcel(String testDataSheetPath, String sheetName) throws DataSheetException {

		Object[][] testDataMapArray = (Object[][]) null;
		dataSheetReader = new DataSheetReader(testDataSheetPath, sheetName);
		testDataMapArray = dataSheetReader.getTestDataMapArray();

		return testDataMapArray;
	}

	public void writeToExcel(LinkedHashMap<String, String> outputData, int rowCount) throws DataSheetException {
		dataSheetReader.writeToExcel(outputData, rowCount);
	}
}

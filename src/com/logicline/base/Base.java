package com.logicline.base;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.lenze.pages.SalesforceHomePage;
import com.lenze.pages.SalesforceLoginPage;
import com.logicline.expections.AutomationException;
import com.logicline.expections.DataSheetException;
import com.logicline.utils.EncriptData;
import com.logicline.utils.Log;



public abstract class Base {

	
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM.dd.YYYY-HH.mm.ss");
	Date reportStamp = new Date();
	String reportTimeStamp = dateTimeFormat.format(reportStamp);
	
	public WebDriver driver = null;
	protected TestDataProvider testDataProvider = null;
	protected  String dataSheetPath = null;
	
	
	public static Properties CONFIG = null;	
	
	public  ExtentHtmlReporter extentHtmlReporter = null;
	public  ExtentReports extentReport = null;
	public  ExtentTest extentTest = null;
	
	protected final Logger logger = Log.getInstance(this.getClass().getSimpleName());

	
	public static boolean isLoggedIn = false;
    public static String token;
    public static String tokensf;
    
    public Base() {
    	
    	CONFIG = new Properties();
    	FileInputStream fs;
		try {
			fs = new FileInputStream("App Settings/config.properties");
			CONFIG.load(fs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		testDataProvider = new TestDataProvider();
    	dataSheetPath = "./Settings/TestDataSheet.xlsx";
	}    

	protected WebDriver getDriver() {
		return driver;
	}
	
	@Parameters({"testBrowser"})
	@BeforeClass	
	protected void beforeClass(String browser) throws AutomationException {
		try {
			extentHtmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/Tests/Reports/"+this.getClass().getSimpleName()+"_"+browser+"_Report_"+reportTimeStamp+".html");
			extentHtmlReporter.setAppendExisting(true);
			extentReport = new ExtentReports();
			System.out.println(extentHtmlReporter);
			extentReport.attachReporter(extentHtmlReporter);
			extentReport.setSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
			extentReport.setSystemInfo("Environment", System.getProperty("os.name").toLowerCase());
			extentReport.setSystemInfo("User Name", System.getProperty("user.name"));
			
			extentHtmlReporter.config().setDocumentTitle(Base.CONFIG.getProperty("projectName"));
			extentHtmlReporter.config().setReportName(Base.CONFIG.getProperty("projectDescription"));
			extentHtmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporter.config().setTheme(Theme.STANDARD);
			
		} catch (UnknownHostException e) {
			logger.error("UnknownHostException occured while trying to get the Host");
			throw new AutomationException("UnknownHostException occured while trying to get the Host");
		}
		
		DriverFactory driverFactory = new DriverFactory();
		driver = driverFactory.getWebDriver(browser);
		if(driver != null)
		{
		driver.manage().timeouts().implicitlyWait(20000, TimeUnit.MILLISECONDS);
		String status=Base.CONFIG.getProperty("enablebackrun");
		if("true".equals(status))
    	{
    		driver.manage().window().setPosition(new Point(-2000, 0));	
    	}
		
		
	driver.get(Base.CONFIG.getProperty("testSiteLadingPageURL"));
	/*	SalesforceLoginPage salesforceLoginPage = new SalesforceLoginPage(driver);
		Assert.assertTrue(salesforceLoginPage.isPageLoaded(), "Salesforce Login page is not displayed");
		salesforceLoginPage.saleforceLogin(Base.CONFIG.getProperty("username"), Base.CONFIG.getProperty("password"));*/
		}
		else
		{
			System.out.println("No driver found");
		}
    
	}
	
    
    @BeforeMethod
    protected void beforeMethod() throws AutomationException {    	

   /* 	SalesforceHomePage salesforceHomePage = new SalesforceHomePage(driver);
    	salesforceHomePage.navigateToCases();    	*/
    }
    
    @AfterMethod(alwaysRun=true)
    protected void afterMethod(ITestResult testResult) throws AutomationException, IOException, DataSheetException {
    	
		
		if(testResult.getStatus() == 1) {
			extentTest.log(Status.PASS, MarkupHelper.createLabel("Test Case is Passed", ExtentColor.GREEN));
			
		} else if(testResult.getStatus() == 2) {
			String file = System.getProperty("user.dir") + "\\" + "/Tests/screenshot" + "\\" + +(new Random().nextInt()) + ".png";
			takeScreenshot(file);
			extentTest.log(Status.FAIL, testResult.getThrowable().getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(file).build());
			 
		} else if(testResult.getStatus() == 3) {
			extentTest.log(Status.SKIP, MarkupHelper.createLabel("Test Case is Skipped", ExtentColor.GREY));
		}
		
    	driver.navigate().refresh();	
    }   
    
    @AfterClass(alwaysRun=true)
    protected void afterClass() throws Exception {
    	extentReport.flush();
    	//driver.close();
		driver.quit();
		String decriptedPassword=CONFIG.getProperty("senderPwd");
		EncriptData obj=new EncriptData();
		decriptedPassword=obj.decrypt(decriptedPassword);
		if(CONFIG.getProperty("sendMail").equalsIgnoreCase("Yes"))
		{
		//sendPDFReportByMail(CONFIG.getProperty("senderID"), decriptedPassword, CONFIG.getProperty("receiverId"), CONFIG.getProperty("mailSubject"), CONFIG.getProperty("mailBody"));
		}
		logger.info("Browser and WebDriver instance are closed");
		
    }
    
    public  void testStep(String testStepName){
    	extentTest.log(Status.INFO, testStepName);
	}
    
    

	public void takeScreenshot(String fileWithPath) throws AutomationException  {
		
		try	{
		// Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot = ((TakesScreenshot) driver);
		// Call getScreenshotAs method to create image file
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		// Create new destination for image file
		File DestFile = new File(fileWithPath);
		// Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
		
		} catch (WebDriverException e) {
			logger.error("Failed to Take screenshot due to WebDriver Exception");
			throw new AutomationException("Failed to Take screenshot due to WebDriver Exception");
			
		} catch (UnsupportedOperationException  e) {
			logger.error("Screenshot capturing is not supported screenshot capturing");
			throw new AutomationException("Screenshot capturing is not supported screenshot capturing");
			
		}catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage() + " occured while trying to take screenshot");
			throw new AutomationException("Exception: " + e.getLocalizedMessage() + " occured while trying to take screenshot");
			
		}
	}
	
	public static void sendPDFReportByMail(String from, String pass, String to, String subject, String body) {
		Properties props = System.getProperties();
		String host = "smtp.1und1.de";
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);

		try {
			// Set from address
			message.setFrom(new InternetAddress(from));
			InternetAddress[] parse = InternetAddress.parse(to , true);
	
			message.addRecipients(Message.RecipientType.TO,parse);
			// Set subject
			message.setSubject(subject);
			message.setText(body);

			BodyPart objMessageBodyPart = new MimeBodyPart();

			objMessageBodyPart.setText(CONFIG.getProperty("mailBody"));

			Multipart multipart = new MimeMultipart();

			multipart.addBodyPart(objMessageBodyPart);

			objMessageBodyPart = new MimeBodyPart();

			// Set path to the pdf report file

			File latest = getLatestFilefromDir(System.getProperty("user.dir") + "/Tests/Reports");

			// Create data source to attach the file in mail
			DataSource source = new FileDataSource(latest);

			objMessageBodyPart.setDataHandler(new DataHandler(source));

			objMessageBodyPart.setFileName(latest.getName());

			multipart.addBodyPart(objMessageBodyPart);

			message.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (AddressException ae) {
			ae.printStackTrace();
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}
	
	static File getLatestFilefromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}

		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;

	}
	

	public static String getIssueID(String issuename) throws JSONException {
		Client client = ClientBuilder.newClient();
		String issueID = null;

		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basicBuilder().build();
		client.register(feature);
		// client.register(Feature.class);
		/*
		 * WebTarget target = client.target(baseurl + "/v1/devices/events/")
		 * .property(HttpAuthenticationFeature.
		 * HTTP_AUTHENTICATION_BASIC_USERNAME, "...")
		 * .property(HttpAuthenticationFeature.
		 * HTTP_AUTHENTICATION_BASIC_PASSWORD, "...");
		 */

		System.out.println(CONFIG);
		System.out.println(CONFIG.getProperty("password"));
		Response response = client.target("https://toolchain.logicline.de/jira/rest/api/latest/issue/" + issuename)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_USERNAME, CONFIG.getProperty("username"))
				.property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_PASSWORD, CONFIG.getProperty("password"))
				.get();

		JSONObject jsonobject = new JSONObject(response.readEntity(String.class));

		Iterator iter = jsonobject.keys();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			if (key.equalsIgnoreCase("id")) {
				issueID = jsonobject.getString(key);

			}

		}
		return issueID;
	}

	public static String getExecutionID(String issueID) throws JSONException {
		Client client = ClientBuilder.newClient();
		String executionID = null;
		Map<String, String> out = new HashMap<String, String>();
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basicBuilder().build();
		client.register(feature);
		// client.register(Feature.class);
		/*
		 * WebTarget target = client.target(baseurl + "/v1/devices/events/")
		 * .property(HttpAuthenticationFeature.
		 * HTTP_AUTHENTICATION_BASIC_USERNAME, "...")
		 * .property(HttpAuthenticationFeature.
		 * HTTP_AUTHENTICATION_BASIC_PASSWORD, "...");
		 */

		;
		Response response = client
				.target("https://toolchain.logicline.de/jira/rest/zapi/latest/execution?projectId="
						+ CONFIG.getProperty("projectID") + "&versionId=" + CONFIG.getProperty("versionID")
						+ "&cycleId=" + CONFIG.getProperty("cycleID") + "&issueID=" + issueID)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_USERNAME, CONFIG.getProperty("username"))
				.property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_PASSWORD, CONFIG.getProperty("password"))
				.get();

		JSONObject jsonobject = new JSONObject(response.readEntity(String.class));
		// JSONObject executions = jsonobject.getJSONObject("executions");
		// JSONObject executions_child = jsonobject.ge
		Iterator iter = jsonobject.keys();
		while (iter.hasNext()) {
			String key = (String) iter.next();

			if (key.equalsIgnoreCase("executions")) {

				// JSONObject executions_count = jsonobject.get(key)[i];
				JSONArray ar = (JSONArray) jsonobject.get(key);
				for (int i = 0; i < ar.length(); i++) {
					JSONObject execution_step = (JSONObject) ar.get(i);

					System.out.println(execution_step);
					Iterator iter2 = execution_step.keys();

					while (iter2.hasNext()) {
						String key2 = (String) iter2.next();
						if (key2.equalsIgnoreCase("issueId")) {
							if (jsonobject.getString(key2).equalsIgnoreCase(issueID)) {
								parse((JSONObject) ar.get(i), out);
							}
						}

					}

				}

			}

		}
		return executionID;

	}

	public static Map<String, String> parse(JSONObject json, Map<String, String> out) throws JSONException {
		Iterator<String> keys = json.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			String val = null;
			try {
				JSONObject value = json.getJSONObject(key);
				parse(value, out);
			} catch (Exception e) {
				val = json.getString(key);
			}

			if (val != null) {
				out.put(key, val);
			}
		}
		return out;
	}

	
}

package com.logicline.base;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;

import com.logicline.expections.AutomationException;
import com.logicline.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;


class DriverFactory {
	final Logger logger = Log.getInstance(this.getClass().getSimpleName());

	private DesiredCapabilities cap;
	/**
	 * This function initialize a WebDriver object based on the Browser type
	 * @author Augustine
	 * @param browser
	 * 				- Browser type in which WebDriver object needs to be initialized
	 * @return WebDriver instance that has been initialized based on the Browser Type
	 */
	
	WebDriver getWebDriver(String browser) {
		WebDriver driver = null;
		if ("Google Chrome".equalsIgnoreCase(browser)) {

			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			options.addArguments("--enable-automation");
			options.addArguments("--disable-extensions");
			options.addArguments("test-type=browser");
			options.addArguments("disable-infobars");
			System.setProperty("webdriver.chrome.driver", "App Settings/Drivers/chromedriver.exe");
			logger.info(
					"webdriver.chrome.driver system property set as: " + System.getProperty("webdriver.chrome.driver"));
			driver = new ChromeDriver(options);
			logger.info("Webdriver object initialized with ChromeDriver instance");

		} else if ("Mozilla Firefox".equalsIgnoreCase(browser)) {
			try
			{

			System.setProperty("webdriver.gecko.driver", "App Settings/Drivers/geckodriver.exe");
			logger.info(
					"webdriver.gecko.driver system property set as: " + System.getProperty("webdriver.gecko.driver"));
			driver = new FirefoxDriver();
			logger.info("Webdriver object initialized with FirefoxDriver instance");
			}
			catch (Exception e) {
			
			}

		} else if ("Microsoft Edge".equalsIgnoreCase(browser)) {
			System.setProperty("webdriver.edge.driver", "App Settings/Drivers/MicrosoftWebDriver.exe");
			logger.info("webdriver.edge.driver system property set as: " + System.getProperty("webdriver.edge.driver"));
			driver = new EdgeDriver();
			logger.info("Webdriver object initialized with EdgeDriver instance");

		} else if ("Safari".equalsIgnoreCase(browser)) {

		} 
		
		else if ("Android".equalsIgnoreCase(browser)) {
			try {
				startServer();
			 DesiredCapabilities capabilities = new DesiredCapabilities();
			 capabilities.setCapability("deviceName", " Android");
			 capabilities.setCapability("device", "Android");
			 capabilities.setCapability("platformName", "Android");
			 capabilities.setCapability("appActivity", "lenze.smart.inventory.mobile.MainActivity");
			 capabilities.setCapability("appPackage", "lenze.smart.inventory.mobile");
		//AppiumDriver<MobileElement> driverMobile=(AppiumDriver<MobileElement>) driver;
		//AppiumDriver<MobileElement>	driverMobile=(AppiumDriver<MobileElement>) new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			 driver=new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities); 
		//	driver=driverMobile;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		


		} 
		
		else if ("Android Chrome".equalsIgnoreCase(browser)) {
			try {
				//startServer();
			 DesiredCapabilities capabilities = new DesiredCapabilities();
			// set the capability to execute test in chrome browser
			 capabilities.setCapability(MobileCapabilityType.BROWSER_NAME,BrowserType.CHROME);
			 
			// set the capability to execute our test in Android Platform
			   capabilities.setCapability(MobileCapabilityType.PLATFORM,Platform.ANDROID);
			 
			// we need to define platform name
			  capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
			  

			  capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"my phone");
		
				driver=new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			
			
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} 

		else {
			logger.error("Browser: " + browser
					+ " is invalid. Please one of the following: \n1.Chrome\n2.Firefox\n3.Safari\n");
			new AutomationException("Browser: " + browser
					+ " is invalid. Please one of the following: \n1.Chrome\n2.Firefox\n3.Safari\n");
		}

		return driver;
		
	}
	
	public void startServer() {Runtime runtime = Runtime.getRuntime();
	try {
		runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
		Thread.sleep(10000);
	} catch (IOException | InterruptedException e) {
		e.printStackTrace();
	}
	}

}

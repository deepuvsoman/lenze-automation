/**
 *
 * Copyright (c) 2016 RapidValue Solutions. All rights reserved.
 *
 * http://www.rapidvaluesolutions.com/accurate/
 *
 * This program and the accompanying materials are confidential and proprietary
 * to RapidValue Solutions Inc. No part of it may be used, circulated, quoted, 
 * or reproduced for distribution outside RapidValue. You are hereby notified 
 * that the use, circulation, quoting, or reproducing of this content is 
 * strictly prohibited and may be unlawful.
 * 
 * mailto: contactus@rapidvaluesolutions.com
 *
 */

package com.logicline.expections;

/**
 * 
 * This class keeps all the string constants that used in the framework
 * 
 * @author Sanoj S
 * @author Ajesh M
 * 
 */

public class AutomationConstants {
	public static final String DRIVER_MISSING = "Driver missing";
	
	
	
}

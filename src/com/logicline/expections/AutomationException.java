package com.logicline.expections;

@SuppressWarnings("serial")
public class AutomationException extends Exception {
	
	private String message = null;
	 
    public AutomationException() {
        super();
    }
 
    public AutomationException(String message) {
        super(message);
        this.message = message;
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
}

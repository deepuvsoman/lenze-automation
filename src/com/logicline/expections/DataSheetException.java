package com.logicline.expections;

@SuppressWarnings("serial")
public class DataSheetException extends Exception{
	
	private String message = null;
	 
    public DataSheetException() {
        super();
    }
 
    public DataSheetException(String message) {
        super(message);
        this.message = message;
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}

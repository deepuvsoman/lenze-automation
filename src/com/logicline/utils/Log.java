package com.logicline.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


public class Log {
	/**
	 * This function configures the Log4j.xml
	 * @param className
	 * @return
	 */
	public static Logger getInstance(String className) {
	    DOMConfigurator.configure("./Logs/log4j.xml");
		return Logger.getLogger(className);
	}
}

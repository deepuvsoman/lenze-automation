package com.logicline.utils;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;


import com.logicline.base.Base;
import com.logicline.expections.AutomationException;

public class RestApiUti {

	String baseEndpoint = "https://login.salesforce.com";
	String urltoken = "https://login.salesforce.com/services/oauth2/token";
	String clevURLLogin = "https://rest.cleverreach.com/v2/login.json";
	String clevURL = "https://rest.cleverreach.com/v2";
	List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	HttpPost post;
	HttpResponse response;
	BufferedReader rd;
	StringBuffer result;
	String line = "";
	HttpGet get;
	JSONObject jsonobj;
	JSONArray jsonarray;
	HttpClient client = HttpClientBuilder.create().build();
	HttpDelete delete;

	public void salesforceConnection() throws ClientProtocolException, IOException {
		client = HttpClientBuilder.create().build();
		post = new HttpPost(urltoken);
		urlParameters.add(new BasicNameValuePair("grant_type", "password"));
		urlParameters.add(new BasicNameValuePair("client_id", Base.CONFIG.getProperty("client_id")));
		urlParameters.add(new BasicNameValuePair("client_secret",
				Base.CONFIG.getProperty("cleint_secret")));
		urlParameters.add(new BasicNameValuePair("username",
				Base.CONFIG.getProperty("username")));
		urlParameters.add(new BasicNameValuePair("password", Base.CONFIG.getProperty("password")));
		urlParameters.add(new BasicNameValuePair("format", Base.CONFIG.getProperty("format")));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		response = client.execute(post);
		System.out.println(response);
		rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			JSONObject responsesf = new JSONObject(line);


			Base.tokensf = responsesf.getString("access_token");
			baseEndpoint = responsesf.getString("instance_url");
			System.out.println(baseEndpoint);
		}
	}


	 public void empolisToken() throws ClientProtocolException, IOException {
			client = HttpClientBuilder.create().build();
			post = new HttpPost(clevURLLogin);
			if (null != urlParameters) {
				urlParameters.clear();
			}

			urlParameters.add(new BasicNameValuePair("client_id", Base.CONFIG.getProperty("clevClientId")));
			urlParameters.add(new BasicNameValuePair("login",
					Base.CONFIG.getProperty("clevUserName")));

			urlParameters.add(new BasicNameValuePair("password", Base.CONFIG.getProperty("clevPassword")));

			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			response = client.execute(post);
			System.out.println(response);
			rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			result = new StringBuffer();


			while ((line = rd.readLine()) != null) {

			
				Base.token = line.replace("\"", "");
				System.out.println(Base.token);
			}
		}

	public JSONObject rest_salesforce(String endpoint, String type)
			throws AutomationException, ClientProtocolException, IOException {

		HttpGet get;

		client = HttpClientBuilder.create().build();
		if (type.equals("get")) {
			get = new HttpGet(baseEndpoint + endpoint);
			get.setHeader("Authorization", "Bearer " + Base.tokensf);

			response = client.execute(get);
		}

		rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		result = new StringBuffer();
		line = rd.readLine();
		System.out.println(line);
		jsonobj = new JSONObject(line);

		System.out.println(line);

	

		return jsonobj;

	}

	public void rest_salesforce_delete(String endpoint)
			throws AutomationException, ClientProtocolException, IOException {

		client = HttpClientBuilder.create().build();

		delete = new HttpDelete(baseEndpoint + endpoint);
		delete.setHeader("Authorization", "Bearer " + Base.tokensf);

		response = client.execute(delete);

		System.out.println(response.getStatusLine());

	}
	
	public String getbaseUrl()
	{
		return baseEndpoint;
	}
	

	
	

	
	public JSONObject rest_salesforce_param(String endpoint, String type,List<NameValuePair> params)
			throws AutomationException, ClientProtocolException, IOException {

		if(!endpoint.endsWith("?"))
		{
			endpoint +="?";
		}
		
		HttpGet get;

		client = HttpClientBuilder.create().build();
		if (type.equals("get")) {
		
			String paramString = params.get(0).getValue();
			String paramName=params.get(0).getName();
			
			get = new HttpGet(baseEndpoint + endpoint+paramName+"="+URLEncoder.encode(paramString));
			get.setHeader("Authorization", "Bearer " + Base.tokensf);

			response = client.execute(get);
		}

		rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		result = new StringBuffer();
		line = rd.readLine();
		System.out.println(line);
		jsonobj = new JSONObject(line);
		
		
		
			

			System.out.println(line);

	

			
		
		return jsonobj;

	}
	
}


package com.logicline.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.logicline.base.Base;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

public class EmpolisRestConnection {

	public Properties CONFIG = new Properties();
	Gson gson = new Gson();

	final Type stringStringMap = new TypeToken<Map<String, String>>() {
	}.getType();
	OkHttpClient client = new OkHttpClient();
	LocalDateTime now = LocalDateTime.now();

	public EmpolisRestConnection()  {
		try
		{
		FileInputStream fs = new FileInputStream("./config.properties");

		CONFIG.load(fs);
		}
		catch (IOException e) {
			// TODO: handle exception
		}

	}

	public  String getToken() throws IOException
	{
	

        RequestBody body = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"),
        		"grant_type=password&username=api@ese4sfdc.1&password=y3Y5U4SbR3rm&client_id=_fxRAzPug65fPwyfDfvN4caxi9Qa&client_secret=wD1sz7orcmuyy2v_11s_BkbCgzYa");
        Request request = new Request.Builder().url(CONFIG.getProperty("tokenurl"))
                                .post(body).addHeader("content-type", "application/x-www-form-urlencoded").build();

        Response response = client.newCall(request).execute();
    
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);       
        }
                   
                    Map<String, String>  map = gson.fromJson(response.body().charStream(), stringStringMap);
       String  token = String.format("Bearer %s", map.get("access_token"));
       
       System.out.println("Token"+token);
		
       return token;
		
       
	}

	public boolean isSessionPaused(String ESESessioninSF) throws IOException {
        OkHttpClient client = new OkHttpClient();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://esc-eu-central-1.empolisservices.com/api/ias/1.0/decisiontrees/project1_p/showpausedsessions").newBuilder();
     
        String url = urlBuilder.build().toString();
 
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                "{\"userType\":\"Experte\"}");
        Request request = new Request.Builder().url(
                                url).post(body).addHeader("content-type", "application/json").addHeader("Authorization", getToken())
                                .build();
        
     /*   final Buffer buffer = new Buffer();
        request.body().writeTo(buffer);
System.out.println(buffer);*/
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
        }

        System.out.println("Paused session"+response);
        //return gson.fromJson(response.body().charStream(), ConceptsResponse.class).getConcepts().stream()
          //                  .map(Concept::getConcept).collect(Collectors.toSet());
  
        ResponseBody responseBody = response.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE); // request the entire body.
        Buffer buffer = source.buffer();
        // clone buffer before reading from it
        String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));
        System.out.println(responseBodyString);
        JSONObject jsonObject = new JSONObject(responseBodyString);
        JSONArray myResponse = jsonObject.getJSONArray("records");
        //JSONArray tsmresponse = (JSONArray) myResponse.get("listTsm");

        ArrayList<String> list = new ArrayList<String>();

        for(int i=0; i<myResponse.length(); i++){
            list.add(myResponse.getJSONObject(i).getString("sessionId"));
            
        }

   return   list.contains(ESESessioninSF);
		
	}

}

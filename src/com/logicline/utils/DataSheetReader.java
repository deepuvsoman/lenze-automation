package com.logicline.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.logicline.expections.DataSheetException;


public class DataSheetReader {
	
	final Logger logger = Log.getInstance(this.getClass().getSimpleName());
	
	private File file = null;
	private FileInputStream fileIn = null;
	private FileOutputStream fileOut = null;
	private Workbook workbook = null;
	private Sheet sheet = null;
	private String sheetName = null;
	
	public DataSheetReader(String dataSheetPath) throws DataSheetException {
		
		try {
			file = new File(dataSheetPath);
			fileIn = new FileInputStream(file);
			workbook = new XSSFWorkbook(fileIn);
		} catch (IOException e) {
			logger.error("IOException occured  while creating File Input stream. Please check the Data sheet Path");
			throw new DataSheetException("IOException occured  while creating File Input stream. Please check the Data sheet Path");
		}
	}
	
	public DataSheetReader(String dataSheetPath, String sheetName) throws DataSheetException {
		try {
			file = new File(dataSheetPath);
			fileIn = new FileInputStream(file);
			workbook = new XSSFWorkbook(fileIn);
			if((workbook.getSheetIndex(sheetName)) >= 0) {
				sheet = workbook.getSheet(sheetName);
				this.sheetName = sheetName;
			} else {
				logger.error("Test data sheet does not contains the sheet: " + sheetName);
				throw new DataSheetException("Test data sheet does not contains the sheet: " + sheetName);
			}
			fileIn.close();
		} catch(IOException e) {
			logger.error("IOException occured  while creating File Input stream. Please check the Data sheet Path");
			throw new DataSheetException("IOException occured  while creating File Input stream. Please check the Data sheet Path");
		} 
	}
	
	int getNumberOfSheets() {
		return workbook.getNumberOfSheets();
	}
	
	public LinkedHashMap<String, String> getTestCases() throws DataSheetException {
		int rows = 0;
		int numberOfSheets = getNumberOfSheets();
		
		LinkedHashMap<String, String> testCases = new LinkedHashMap<String, String>();
		
		for(int i=0; i<numberOfSheets; i++) {
			int testCaseIndex = 0;
			sheet = workbook.getSheetAt(i);
			rows = getRowCount();
			if(validateHeading()) {
				for(int row=1; row<rows; row++) {	
					
					testCases.put(sheet.getRow(row).getCell(0).getStringCellValue(), sheet.getRow(row).getCell(2).getStringCellValue().trim() + " : " 
												+ sheet.getRow(row).getCell(3).getStringCellValue().trim() + " :- " 
												+ sheet.getRow(row).getCell(4).getStringCellValue().trim());
					testCaseIndex++;
					
				}
			}
		}
		return testCases;
	}
	
	public void setTestCases(List<String> testCaseIndexList) throws DataSheetException {
		for(int row = 1; row < getRowCount(); row++) {
			   sheet.getRow(row).getCell(1).setCellValue("No");
			  }
		
		for(String testCaseIndex : testCaseIndexList) {
			for( int row = 1; row < getRowCount(); row++) {
				System.out.println(sheet.getRow(row).getCell(0).getStringCellValue());
				System.out.println(sheet.getRow(row).getCell(1).getStringCellValue());
				if(sheet.getRow(row).getCell(0).getStringCellValue().equals(testCaseIndex)) {
					sheet.getRow(row).getCell(1).setCellValue("Yes");
				}
			}
		}
		
		try {
			fileOut = new FileOutputStream(file);
			workbook.write(fileOut);
			fileOut.close();
		} catch (IOException e) {
			logger.error("IOException occured  while creating File Output stream. Please check the Data sheet Path");
			throw new DataSheetException("IOException occured  while creating File Output stream. Please check the Data sheet Path");
		}
		
	}
	
	/**
	 * This method verifies the presence of Execution Order, To be Executed Status and Browser Columns
	 * @author Augustine
	 * @param sheetname
	 *            -name of the sheet in the Excel File
	 * @return- True if Execution Order, To be Executed Status and Browser Columns Columns are present in the data sheet, False otherwise
	 */
	boolean validateHeading() {
		if ((sheet.getRow(0).getCell(0).getStringCellValue().trim().equalsIgnoreCase("ID"))
				&& (sheet.getRow(0).getCell(1).getStringCellValue().trim().equalsIgnoreCase("Execute"))
				&& (sheet.getRow(0).getCell(2).getStringCellValue().trim().equalsIgnoreCase("User Story"))
				&& (sheet.getRow(0).getCell(3).getStringCellValue().trim().equalsIgnoreCase("Test Case ID"))
				&& (sheet.getRow(0).getCell(4).getStringCellValue().trim().equalsIgnoreCase("Scenario"))				
				) {
			return true;
		} else {
			return false;
		}	
	}
	
	/**
	 * This method gets the number of rows in the data sheet
	 * @author Augustine
	 * @return-This method returns the row count in the sheet
	 * @throws DataSheetException
	 */
	int getRowCount() throws DataSheetException {
		int rowCount = 0;		
		rowCount = sheet.getPhysicalNumberOfRows();
		if(rowCount > 0) {
			return rowCount;
		} else {
			logger.error("Test data sheet: " + sheetName + " is blank");
			throw new DataSheetException("Test data sheet: " + sheetName + " is blank");
		}	 
	}
	
	/**
	 * This method gets the number of columns in the data sheet
	 * @author Augustine
	 * @return - column count in the data sheet
	 * @throws DataSheetException 
	 */
	int getColumnCount() throws DataSheetException {
		int columnCount = 0;
		columnCount = sheet.getRow(0).getLastCellNum();
		if(columnCount > 0) {
			return columnCount;
		} else {
			logger.error("Test data sheet: " + sheetName + " is blank");
			throw new DataSheetException("Test data sheet: " + sheetName + " is blank");
		}		
	}
	
	/**
	 * This method gets the number of rows to be executed considering the execution status from the data sheet
	 * @author Augustine
	 * @return - the number of rows to be executed considering the execution status
	 */

	int getRowsToBeExecuted(int rows) throws DataSheetException {
		int count = 0;
		for (int row = 1; row < rows; row++) {
			if (sheet.getRow(row).getCell(1).getStringCellValue().trim().equalsIgnoreCase("Yes")) {
				count++;
			}
		}
		if(count > 0) {
			return count;
		} else {
			logger.error("There are no test case rows to be executed in the sheet: " + sheetName);
			throw new DataSheetException("There are no test case rows to be executed in the sheet: " + sheetName);
		}
		
	}
	
	/**
	 * This method reads Test Data from Excel files and returns as 2- dimensional object array
	 * @author Augustine
	 * @param sheetname
	 *            - name of the sheet in the Excel File
	 * @return - 2- dimensional data object array
	 * @throws DataSheetException 
	 */
	public Object[][] getTestDataMapArray() throws DataSheetException {

		int columns = getColumnCount();
		int rows = getRowCount();
		int dataObjectArraySize = getRowsToBeExecuted(rows);
		Object[][] dataObjectArray = new Object[dataObjectArraySize][1];
		int index = 0;
		
		// Validating for the heading status 
		if (validateHeading()) {
			// Iterates through each row in excel 
			for (int row = 1; row < rows; row++) {
				String executionStatus=sheet.getRow(row).getCell(1).getStringCellValue().trim();
				 //Checking if execution status is 'Y' 
				if (executionStatus.equalsIgnoreCase("Yes")) {
					LinkedHashMap<String, String> linkedDataMap = new LinkedHashMap<String, String>();
					for (int column = 0; column < columns; column++) {
						/*if(sheet.getRow(row).getCell(column).getCellType() == Cell.CELL_TYPE_STRING) {
							
							linkedDataMap.put(sheet.getRow(0).getCell(column).getStringCellValue().trim(), sheet.getRow(row).getCell(column).getStringCellValue().trim());
							
						}*/ if(sheet.getRow(row).getCell(column).getCellType() == Cell.CELL_TYPE_NUMERIC) {
							
							linkedDataMap.put(sheet.getRow(0).getCell(column).getStringCellValue().trim(), String.valueOf(sheet.getRow(row).getCell(column).getNumericCellValue()).trim());
							
						} else {
							linkedDataMap.put(sheet.getRow(0).getCell(column).getStringCellValue().trim(), sheet.getRow(row).getCell(column).getStringCellValue().trim());
						}
							/*else {
						
							logger.error("Value in the Cell["+row+"]["+column+"] is neither String nor Numeric. Cell values needs to be String or numeric");
							throw new DataSheetException("Value in the Cell["+row+"]["+column+"] is neither String nor Numeric. Cell values needs to be String or numeric");
						}*/
					}
					dataObjectArray[index] = new Object[] { linkedDataMap };
					linkedDataMap.put("rowCount",Integer.toString(row));
					index++;
				} else if(executionStatus.equalsIgnoreCase(""))	{
					logger.error("Execute Column in Row: " + row + " is empty. Please enter either Yes or No in Execute column");
					throw new DataSheetException("Execute Column in Row: " + row + " is empty. Please enter either Yes or No in Execute column");	
				}
			}
		}else {
			logger.error("The sheet headings are invalid:The headings should be Browser and Execution Status");
			throw new DataSheetException("The sheet headings are invalid:The headings should be Browser and Execution Status");
			}
		return dataObjectArray;
	}
	
	public void writeToExcel(LinkedHashMap<String, String> outputData, int rowCount) throws DataSheetException {
		Cell cell = null;
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());

		int newColumn = 0;

		for(String key: outputData.keySet()) {

			String data = outputData.get(key);
			boolean keyExist = false;
			for(int i=0; i < getColumnCount(); i++){

				String cloumnHeading = sheet.getRow(0).getCell(i).getStringCellValue().trim();
				

				if(key.equalsIgnoreCase("Failure Message")){
					style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

				}

				if(data.equalsIgnoreCase("Fail")){							
					style.setFillForegroundColor(IndexedColors.RED.getIndex());


				} else if(data.equalsIgnoreCase("Pass")){							
					style.setFillForegroundColor(IndexedColors.GREEN.getIndex());


				} else if(data.equalsIgnoreCase("Skip")){							
					style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());

				}

				if(cloumnHeading.equalsIgnoreCase(key)) {
					
					keyExist = true;
					
					style.setFillPattern(CellStyle.SOLID_FOREGROUND);
					cell = sheet.getRow(rowCount).getCell(i);
					cell.setCellStyle(style);
					cell.setCellValue(data);
					
					break;
				}										
			}

			if(!keyExist) {
				
				style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell = sheet.getRow(0).createCell(getColumnCount() + newColumn);
				cell.setCellStyle(style);
				cell.setCellValue(key);
				
				style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell = sheet.getRow(rowCount).createCell(getColumnCount() + newColumn);
				cell.setCellStyle(style);
				cell.setCellValue(data);
				
				newColumn++;
			}
		}

		try {
			fileOut = new FileOutputStream(file);
			workbook.write(fileOut);
			fileOut.close();
		} catch (IOException e) {
			logger.error("IOException occured  while creating File Output stream. Please check the Data sheet Path");
			throw new DataSheetException("IOException occured  while creating File Output stream. Please check the Data sheet Path");
		}
	}
}

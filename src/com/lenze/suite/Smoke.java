package com.lenze.suite;


import org.testng.annotations.DataProvider;


import org.testng.annotations.Test;

import com.logicline.base.Base;
import com.logicline.expections.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;

import org.apache.tools.ant.types.resources.selectors.InstanceOf;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Smoke extends Base {
	
	
	@DataProvider(name="dataSheet")
	public Object[][] getTestDataForExecution() throws DataSheetException {
		
		return testDataProvider.getTestDataFromExcel(dataSheetPath, this.getClass().getSimpleName()); 
	}

	@Test(dataProvider = "dataSheet")

	void testMethod(LinkedHashMap<String, String> testDataLinkedHashMap) throws DataSheetException, AutomationException {
		String s=null;
		if(driver instanceof ChromeDriver)
		
			s="chrome";
		else if (driver instanceof FirefoxDriver) 
			s="firefox";
		
		
		extentTest = extentReport.createTest(testDataLinkedHashMap.get("User Story") + ": " + testDataLinkedHashMap.get("Test Case ID"), testDataLinkedHashMap.get("Scenario")  + s );
		
		logger.info("Test started for Test Case: " + testDataLinkedHashMap.get("Test Case ID") + " in User Story: " + testDataLinkedHashMap.get("User Story"));
		
		try {
			Class<?> c = Class.forName("com.lenze.scripts." + testDataLinkedHashMap.get("User Story"));
			Object obj = c.getDeclaredConstructor(WebDriver.class).newInstance(getDriver());
			Method methods[] = obj.getClass().getDeclaredMethods();
			for(Method method: methods) { 

				if(method.getName().equalsIgnoreCase(testDataLinkedHashMap.get("Test Case ID"))) {
					method.invoke(obj, testDataLinkedHashMap,extentTest);
				}
			}
		} catch (ClassNotFoundException e) {
			logger.error("ClassNotFoundException occured, please check the User Story in the Data sheet");
			throw new AutomationException("ClassNotFoundException occured, please check the User Story in the Data sheet");
		} catch (InstantiationException e) {
			logger.error("Unable to instantiate the Class: " + testDataLinkedHashMap.get("User Story"));
			throw new AutomationException("Unable to instantiate the Class: " + testDataLinkedHashMap.get("User Story"));
		} catch (IllegalAccessException e) {
			logger.error("Unable to access the Class: " + testDataLinkedHashMap.get("User Story") + "or Method: " + testDataLinkedHashMap.get("Test Case ID"));
			throw new AutomationException("Unable to access the Class: " + testDataLinkedHashMap.get("User Story") + "or Method: " + testDataLinkedHashMap.get("Test Case ID"));
		} catch (InvocationTargetException e) {
			logger.error(e.getCause().getMessage());
			throw new AutomationException(e.getCause().getMessage());
		} catch (NoSuchMethodException e) {
			logger.error("Please define the Constructor with WebDriver as the parameter the Class: " + testDataLinkedHashMap.get("User Story"));
			throw new AutomationException("Please define the Constructor with WebDriver as the parameter the Class: " + testDataLinkedHashMap.get("User Story"));
		} catch (SecurityException e) {
			logger.error("SecurityException occured while trying to get declared constructor or getting declared methods in the Class: " + testDataLinkedHashMap.get("User Story"));
			throw new AutomationException("SecurityException occured while trying to get declared constructor or getting declared methods in the Class: " + testDataLinkedHashMap.get("User Story"));
		}
		
		logger.info("Test finished for Test Case: " + testDataLinkedHashMap.get("Test Case ID") + " in User Story: " + testDataLinkedHashMap.get("User Story"));
	}	

}

package com.lenze.pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.logicline.base.Operations;
import com.logicline.expections.AutomationException;

public class Loginpage extends Operations{
	
	private By username = getLocator(LocatorType.BY_XPATH, "//INPUT[@id='email']");
	private By password_1 = getLocator(LocatorType.BY_XPATH, "//INPUT[@id='password']");
	private By username_mobile =getLocator(LocatorType.BY_XPATH, "//android.view.View[@resource-id='lbl-0']/following-sibling::android.view.View//android.widget.EditText");
	private By password_mobile =getLocator(LocatorType.BY_XPATH, "//android.view.View[@resource-id='lbl-1']/following-sibling::android.view.View//android.widget.EditText");
	private By login_button_mobile=getLocator(LocatorType.BY_XPATH, "//android.widget.Button");
	private By login_button=getLocator(LocatorType.BY_XPATH, "//BUTTON[text()='Login']");
	private By isLoggedinVerification=getLocator(LocatorType.BY_XPATH, "//*[text()='Logout']");
	private By isLoggedinVerificationMobile=getLocator(LocatorType.BY_XPATH, "//android.widget.Button");
	
			public Loginpage(WebDriver driver){
		super(driver);
	}
			
			public void login(String userName, String password) throws AutomationException {
				
				if(driver instanceof RemoteWebDriver && ((RemoteWebDriver) driver).getCapabilities().getCapability("browserName")==null )
				{
					try {
						Thread.sleep(5000);
						System.out.println(((RemoteWebDriver) driver).getCapabilities());
						enterText(username_mobile, userName);
						enterText(password_mobile, password);
						click(login_button_mobile);
						//assertTrue(islogged());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
			
				}
				else
				{
					enterText(username, userName);
					enterText(password_1, password);
					click(login_button);
					//assertTrue(islogged());
				}
				
			
				
			}
			
	public boolean islogged() throws AutomationException {
				
		if(driver instanceof RemoteWebDriver && ((RemoteWebDriver) driver).getCapabilities().getCapability("browserName")==null )
		{
			System.out.println(isLoggedinVerificationMobile);
			return isElementVisible(isLoggedinVerificationMobile);
			
		}
		
		else
		{
			return isElementVisible(isLoggedinVerification);
		}
				
			
				
			}
}

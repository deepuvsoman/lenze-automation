package com.lenze.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.logicline.base.Operations;
import com.logicline.expections.AutomationException;

public class CasesPage extends Operations{
	
	private By newButton = getLocator(LocatorType.BY_XPATH, "//div[text()='New']");
	private By statusDropdown = getLocator(LocatorType.BY_CSSSELECTOR, "a[aria-label='Status']");
	private By caseOriginDropdown = getLocator(LocatorType.BY_XPATH, "//SPAN[text()='Case Origin']/../..//A");
	private By saveButton = getLocator(LocatorType.BY_CSSSELECTOR, "button[title='Save']");

	public CasesPage(WebDriver driver){
		super(driver);
	}
	
	public void createNewCase(String caseOrigin) throws AutomationException {
		click(newButton);
		click(caseOriginDropdown);
		click(getLocator(LocatorType.BY_XPATH, "//a[text()='"+caseOrigin+"']"));
		click(saveButton);
	}
	
	public void createNewCaseWithStatus(String caseStatus, String caseOrigin) throws AutomationException {
		click(newButton);
		click(statusDropdown);
		click(getLocator(LocatorType.BY_XPATH, "//a[text()='"+caseStatus+"']"));
		click(caseOriginDropdown);
		click(getLocator(LocatorType.BY_XPATH, "//a[text()='"+caseOrigin+"']"));
		click(saveButton);
	}
		
}

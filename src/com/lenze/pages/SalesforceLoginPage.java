package com.lenze.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.logicline.base.Operations;
import com.logicline.expections.AutomationException;

public class SalesforceLoginPage extends Operations{
	
	 private By userNameField = getLocator(LocatorType.BY_ID, "username");
	 private By passwordField = getLocator(LocatorType.BY_ID, "password");
	 private By loginButton = getLocator(LocatorType.BY_ID, "Login");

	public SalesforceLoginPage(WebDriver driver){
		super(driver);
	}
	
	public boolean isPageLoaded() throws AutomationException {
		
		if((isElementVisible(userNameField)) && (isElementVisible(passwordField)) && (isElementVisible(loginButton))) {
			
			return true;
		} else {
			
			return false;
		}
	}
	
	
	public void saleforceLogin(String userName, String password) throws AutomationException {
		enterText(userNameField, userName);
		enterText(passwordField, password);
		click(loginButton);		
	}
}

package com.lenze.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.logicline.base.Operations;
import com.logicline.expections.AutomationException;

public class SalesforceHomePage extends Operations{
	
	private By salesForceLogo = getLocator(LocatorType.BY_CSSSELECTOR, "div[class='slds-global-header__item']");
	private By homeTab = getLocator(LocatorType.BY_CSSSELECTOR, "a[title='Home'][tabindex='0']");
	private By appLauncherButtom = getLocator(LocatorType.BY_CSSSELECTOR, "button.slds-button>div.slds-icon-waffle");
	private By casesLink = getLocator(LocatorType.BY_CSSSELECTOR, "a[title='Cases'].app-launcher-link");

	public SalesforceHomePage(WebDriver driver){
		super(driver);
	}
	
	public void clickHomeTab() throws AutomationException {
		click(homeTab);
	}
	
	public void navigateToCases() throws AutomationException {
		click(appLauncherButtom);
		click(casesLink);
	}
}

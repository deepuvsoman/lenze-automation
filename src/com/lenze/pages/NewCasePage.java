package com.lenze.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.logicline.base.Operations;
import com.logicline.expections.AutomationException;

public class NewCasePage extends Operations{
	
	private By detailsHeader = getLocator(LocatorType.BY_XPATH, "//span[text()='Details']");
	private By esesessionID = getLocator(LocatorType.BY_XPATH, "//SPAN[text()='ESESessionId']/../..//SPAN[@data-aura-class='uiOutputText']");

	public NewCasePage(WebDriver driver){
		super(driver);
	}
	
	public boolean isDetailsHeaderDisplayed() throws AutomationException {
		return isElementVisible(detailsHeader);
	}
	
	public String getESESessionID() throws AutomationException {
		return getText(esesessionID);
	}
		
}
